package com.example.restaurants;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface RestaurantRepository extends MongoRepository<Restaurant, Long> {

	List<Restaurant> findTop5byNameOrderByName();
}