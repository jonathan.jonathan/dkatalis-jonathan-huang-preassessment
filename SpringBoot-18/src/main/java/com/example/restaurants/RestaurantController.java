package com.example.restaurants;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/restaurant")
public class RestaurantController {

    @Autowired
    private RestaurantRepository restaurantRepository;

    @GetMapping("/getTop5")
    public ResponseEntity<List<Restaurant>> getTop5() {
    	List<Restaurant> listRestaurant = restaurantRepository.findTop5byNameOrderByName();
    	return new ResponseEntity<List<Restaurant>>(listRestaurant, HttpStatus.OK);
    }

}
